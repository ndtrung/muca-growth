# Makefile for MUCA growth

CXX = mpic++
CFLAGS = -g -O2 -march=native -ffast-math -funroll-loops
LINK_FLAGS = -g -O2
LIBS =
AR = ar

SRC = muca_growth.cpp
OBJS = ${SRC:.cpp=.o}

EXECS = run_muca

all: $(EXECS)

muca_growth.o: muca_growth.cpp muca_growth.h
	$(CXX) $(CFLAGS) -c $<

main.o: main.cpp
	$(CXX) $(CFLAGS) -c $<

run_muca: $(OBJS) main.o
	$(CXX) $(LINK_FLAGS) $(OBJS) main.o $(LIBS) -o run_muca

clean:
	rm -f *.o $(EXECS)

