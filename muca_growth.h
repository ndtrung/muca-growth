/*
  Implementing the Multicanonical growth algorithm, last update August 1, 2018
 
  adapted from Orland & Garel 1990
  from discussions of H.Orland, G.Vernizzi (Saclay 5-9/2017)
  implementation by T.Nguyen & G.Vernizzi  (NU 8-10/2017)
  supported by M. Olvera de la Cruz (while at Northwestern, 7-12/2017)
  supported by H. Orland (while at Saclay, 5-9/2017)

  Contact: Trung Nguyen (Northwestern, ndactrung@gmail.com)
*/
#include <cstdlib>
#include <cstdint>

#ifndef __MUCA_GROWTH
#define __MUCA_GROWTH

namespace MC
{

struct Confinement {
  double radius;            // of sphere at (0,0,0), or cylinder along z axis
  double zlo, zhi;          // of plane parallel to x-y plane at z = 0
};

class MUCAGrowth
{
  public:
    MUCAGrowth(int argc, char** argv);
    MUCAGrowth(int dimension, int num_monomers, int population_size,
      int _seq=0, double _energy=-1.0, int _seed=198411);
    ~MUCAGrowth();

    // one-time setting and memory allocation
    void init();

    // setup for each run, reset the arrays
    void setup();

    // top-level run
    void run();

    // perform a single run
    void single_run();

    // perform multiple runs and compute averages in g(E) and C_v
    void multi_runs();

    // return the energy of the added monomer
    virtual inline double compute_energy(double*, int, int, double);
    // workhorse for run(), perform the growth from N = 1 to N = L monomers
    void ensemble_growth();
    void ensemble_growth2();

    // set the cutoff for the non-bonded interaction
    inline void set_cutoff(double _cut) { cutoff = _cut; cutsq = cutoff*cutoff; }
    // set the absolute tolerance for population size
    inline void set_abs_diff(int _diff) { abs_diff = _diff; }
    // turn on/off writing out g(E) and H(E) at every stage
    inline void set_write_every(int _write_every) { write_every = _write_every; }
    // turn on/off writing out the lowest energy configurations at the final stage
    inline void set_write_final(int _write_final) { write_final = _write_final; }
    // set the temperature range and deltaT for specific heat computation
    inline void set_temperature(double _Tmin, double _Tmax, double _dT) {
       Tmin = _Tmin; Tmax = _Tmax; dT = _dT;
    }
    // set fixed size population or not
    inline void set_fixed_size(int _fixed_size) { fixed_size = _fixed_size; }
    // set the debug level to examine memory (re)allocation and output
    inline void set_debug_level(int _level) { debug_level = _level; }
    // set the histogram flatness tolerance (no longer applicable)
    inline void set_flat_tolerance(double _tol) { flat_tolerance = _tol; }
    // set log file
    void set_log(const char* _filename);

    // compute maximum memory usage during the run
    inline double memory_usage();

  protected:
    // generate all the daughters from the parents at stage n
    uint64_t generate_daughters(int n);
    // compute the energy histogram of the selected daughters
    void compute_energy_histogram();
    // compute the energy histogram of the selected daughters with their weights
    void compute_weighted_energy_histogram(int n);
    // estimate the number of daughters to be replicated to rescale gamma's
    double estimate_num_daughters(int n);
    // select M daughters at stage n using gE (parents), g (daughters) and gamma
    uint64_t select_daughters(int n);
    // save the selected daughter configurations into the current parents for the next stage
    void update_configurations(int n);
    // apply confinement to the trial configuration
    int apply_confinement(double* t, double& eng, int n);

    // write out configurations
    void write_dump();
    // write out g(E), norm(g(E)) and H(E)
    void output_gE(int n, int irun=0);
    // compute and write out C_v
    void compute_cv(int, int irun=0);
    // grow arrays by chunks when the newly added daughters exceed the allocated size
    void grow_arrays(int);
    // check if the energy histogram is flat (using Kullback-Leibler algorithm)
    int check_flatness(double*, int, double, double&);

    int dim;                  // lattice dimension
    int NA;                   // coordination number of the lattice
    int L;                    // target number of monomers
    int seq;                  // sequence type
    int* mtypes;              // mtypes[i] = type of mommer i
    int seed;                 // seed for the RNG
    double pair_energy[2][2]; // pairwise energy between non-bonded monomers
    double energy;            // interaction energy
    double cutoff;            // interaction cutoff
    double cutsq;             // = cutoff * cutoff

    int M0;                   // target number of configurations
    int M;                    // actual number of configurations at a stage
    int64_t K0;               // total number of self-avoiding generated daughters 
    int abs_diff;             // absolute difference in population size to be considered close to M0
    double rel_diff;          // relative difference in population size to be considered close to M0
    int fixed_size;           // 1 = attempt to fix the population size to M0 (default),
                              // 0 = keep all the generated daughters

    double Ebmin, Ebmax;      // lower and upper bounds for the chain of final length L
    int ntotal_bins;          // total number of bins for [Ebmin;Ebmax]
    double Emin, Emax, dE;    // Emin and Emax for all the daughters K0, and bin width
    double* H;                   // energy histogram
    double* buffer;              // buffer histogram
    int Nbins;                // number of bins from [Emin;Emax]
    double Emin_parent;       // Emin of the parent configurations to compute gE[ibin_parent]
    int nbins_parent;         // number of bins of the parent configurations 

    double*** polymer;        // parent configurations polymer[i][j][k] = coordinate k of monomer j in chain i
    double*** NewPolymer;     // daughter configurations polymer[i][j][k] = coordinate k of monomer j in chain i
    double* E;                // energy of the parent configurations
    double* Enew;             // Enew[k] = energy of daughter k in all the generated daughters K0 array
    double** move;            // move[k][0] = index of the parent configuration of daughter k
                              // move[k][1] = direction of the growth from the parent
    int64_t* ind;             // ind[k] = index of the selected daughter in the all daughter array K0
    double* gamma;            // population control factor
    double** d;               // direction vectors on the lattice
    double flat_tolerance;    // tolerance for flatness
    double* gE;               // density of states of the parents
    double* g;                // density of states of the daughters to be computed
    double acc_gamma;         // product of all gamma's

    double Tmin, Tmax, dT;    // for specific computation
    int ntemp_bins;           // number of temperature bins
    double* Cv;               // specific heat for [Tmin;Tmax]

    int confinement_mode;
    Confinement confinement;

    int num_runs;
    int write_final;          // 1 = write out final configurations at L
    int write_every;          // 1 = write out g(E) and H(E) at every stage
    int log_every;            // write out to log/screen every number of stages
    int64_t allocated_size;   // current allocated size for the arrays Enew and move
    int64_t allocated_size2;  // current allocated size for the other arrays
    int debug_level;          // debug level: 0 = no info, 1 = stage, 2 = memory reallocation
    int scheme;               // 0 = former scheme using meanH; 1 = latest scheme using the support of gE

    int usinglog;             // 1 if using log file for screen output
    FILE* logfile;            // pointer to the file
    char* logfilename;        // log file name

    bool _params_ready;
    int me,nprocs;

  public:
    // read in an exact enumeration of g(E) to compute Cv
    void testing_compute_cv(const char* filename, int nbins, int start_bin=0);

};

}
#endif
