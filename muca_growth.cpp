/*
  Implementing the Multicanonical growth algorithm, last update November 29, 2019
 
  Updated the algorithm with the weighted energy historgram and the support of g(E) for computing the weights
  Supported options for spherical or cylindrical confinements

  adapted from Orland & Garel 1990
  from discussions of H.Orland, G.Vernizzi (Saclay 5-9/2017)
  implementation by T.Nguyen & G.Vernizzi  (NU 8-10/2017)
  supported by M. Olvera de la Cruz (while at Northwestern, 7-12/2017)
  supported by H. Orland (while at Saclay, 5-9/2017)
*/

#include <mpi.h>
#include <cstring>
#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <cfloat>
#include <ctime>
#include "muca_growth.h"
#include "memory.h"

#define MAXLEN 14
#define DELTA 1000
#define EPSILON 0.01

// uint64_t fixed-size 64-bit integer, INT64_MAX = 2^64 - 1
//#define _MUCA_DEBUG

int types0[MAXLEN] = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0};
int types1[MAXLEN] = {0,1,0,1, 0,0,1,0, 1,0,0,1, 1,0};
int types2[MAXLEN] = {0,0,1,1, 0,1,0,1, 0,0,1,0, 1,0};
int types3[MAXLEN] = {0,0,1,0, 1,0,1,1, 0,1,0,1, 0,0};
int types4[MAXLEN] = {0,0,1,0, 1,1,0,1, 0,1,0,0, 1,0};

enum {CONFINEMENT_NONE=0,CONFINEMENT_SPHERE=1,CONFINEMENT_PLANE=2,CONFINEMENT_CYLINDER=3};

using namespace MC;
using namespace std;

/*---------------------------------------------------------------------------*/

MUCAGrowth::MUCAGrowth(int argc, char** argv)
{
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &me);

  polymer = NULL;
  NewPolymer = NULL;
  E = NULL;
  Enew = NULL;
  move = NULL;
  ind = NULL;
  mtypes = NULL;
  gamma = NULL;
  H = NULL;
  buffer = NULL;
  g = NULL;
  gE = NULL;
  Cv = NULL;
  logfilename = NULL;

  // default values
  dim = 3;
  L = 14;
  M0 = 100;
  seed = 1984321;
  dE = 1.0;
  Tmin = 0.01;
  Tmax = 3.0;
  dT = 0.01;

  confinement_mode = 0;

  num_runs = 1;
  seq = 0;
  energy = -1;
  write_final = 0;
  write_every = 0;
  log_every = 1;
  flat_tolerance = 1.0e-4;  
  fixed_size = 1;
  rel_diff = 0.1;
  abs_diff = 0;
  debug_level = 0;
  usinglog = 1;
  scheme = 0;

  char* filename = (char*)"log.txt";
  int len = strlen(filename);
  logfilename = new char [len+1];
  strcpy(logfilename, filename);
  
  _params_ready = true;

  int iarg = 1;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"dimension") == 0) {
      dim = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"num_monomers") == 0) {
      L = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"replicas") == 0) {
      M0 = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"pair_energy") == 0) {
      energy = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"sequence") == 0) {
      seq = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"temperature") == 0) {
      if (iarg+4 > argc) std::cout << "Invalid argument for temperature\n";
      Tmin = atof(argv[iarg+1]);
      Tmax = atof(argv[iarg+2]);
      dT = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"confinement") == 0) {
      if (iarg+3 > argc) std::cout << "Invalid argument for confinement\n";
      if (strcmp(argv[iarg+1],"sphere") == 0) {
        confinement_mode = CONFINEMENT_SPHERE;
        confinement.radius = atof(argv[iarg+2]);
        iarg += 3;
      } else if (strcmp(argv[iarg+1],"plane") == 0) {
        confinement_mode = CONFINEMENT_PLANE;
        confinement.zlo = atof(argv[iarg+2]);
        confinement.zhi = atof(argv[iarg+3]);
        iarg += 4;
      } else if (strcmp(argv[iarg+1],"cylinder") == 0) {
        confinement_mode = CONFINEMENT_CYLINDER;
        confinement.radius = atof(argv[iarg+2]);
        iarg += 3;
      } else std::cout << "Invalid argument for confinement\n";

    } else if (strcmp(argv[iarg],"seed") == 0) {
      seed = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"num_runs") == 0) {
      num_runs = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"write_final") == 0) {
      write_final = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"write_every") == 0) {
      write_every = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"log_every") == 0) {
      log_every = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"flat_tol") == 0) {
      flat_tolerance = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"debug_level") == 0) {
      debug_level = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"fixed_size") == 0) {
      fixed_size = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"population_tol") == 0) {
      rel_diff = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"num_runs") == 0) {
      num_runs = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"scheme") == 0) {
      scheme = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"log") == 0) {
      usinglog = 1;
      if (logfilename) delete [] logfilename;
      int len = strlen(argv[iarg+1]);
      logfilename = new char [len+1];
      strcpy(logfilename, argv[iarg+1]);
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      _params_ready = false;
    }
  }

  // sanity checks
  if (L <= 1) {
    std::cout << "Chain length should be greater than 1.\n";
    _params_ready = false;
  }

  if (dim != 2 && dim != 3) {
    std::cout << "Invalid value for lattice dimension.\n";
    _params_ready = false;
  }

  if (L > 14 && seq != 0) {
    std::cout << "Sequence " << seq << " has fewer than 14 monomers.\n";
    _params_ready = false;
  }
}

/*---------------------------------------------------------------------------*/

MUCAGrowth::MUCAGrowth(int dimension, int num_monomers, int population_size,
  int _seq, double _energy, int _seed) :  dim(dimension), L(num_monomers),
  M0(population_size), seq(_seq), energy(_energy), seed(_seed)
{
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &me);

  polymer = NULL;
  NewPolymer = NULL;
  E = NULL;
  Enew = NULL;
  move = NULL;
  ind = NULL;
  mtypes = NULL;
  gamma = NULL;
  H = NULL;
  buffer = NULL;
  g = NULL;
  gE = NULL;
  Cv = NULL;
  logfile = NULL;
  logfilename = NULL;

  // default values
  
  num_runs = 1;
  write_final = 0;
  write_every = 0;
  flat_tolerance = 1.0e-4;  
  fixed_size = 1;
  debug_level = 0;
  usinglog = 0;

  energy = -1;
  dE = 1.0;
  Tmin = 0.01;
  Tmax = 3.0;
  dT = 0.01;
  
  write_final = 1;
  write_every = 0;
  log_every = 1;
  fixed_size = 1;
  debug_level = 0;
  
  char* filename = (char*)"log.txt";
  int len = strlen(filename);
  logfilename = new char [len+1];
  strcpy(logfilename, filename);

  _params_ready = true;
}

/*---------------------------------------------------------------------------*/

MUCAGrowth::~MUCAGrowth()
{
  destroy(polymer);
  destroy(NewPolymer);
  destroy(E);
  destroy(Enew);
  destroy(move);
  destroy(ind);
  destroy(gE);
  destroy(g);
  if (seq == 0) destroy(mtypes);
  destroy(gamma);
  destroy(H);
  destroy(d);
  destroy(Cv);
  destroy(buffer);
  if (logfilename) delete [] logfilename;
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::set_log(const char* _filename)
{
  usinglog = 1;
  strcpy(logfilename, _filename);
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::init()
{
  Ebmin = -2*L;
  Ebmax = 0;
  Emin = Emax = 0;

  // NA = coordination number of the lattice
  NA = 2*dim;
  create(d, NA, dim);
  if (dim == 3) {
    // d[NA][DIM] = {{1, 0, 0}, {-1, 0, 0}, {0, 1, 0}, {0, -1, 0}, {0, 0, 1}, {0, 0, -1}};
    d[0][0] =  1; d[0][1] =  0; d[0][2] =  0;
    d[1][0] = -1; d[1][1] =  0; d[1][2] =  0;
    d[2][0] =  0; d[2][1] =  1; d[2][2] =  0;
    d[3][0] =  0; d[3][1] = -1; d[3][2] =  0;
    d[4][0] =  0; d[4][1] =  0; d[4][2] =  1;
    d[5][0] =  0; d[5][1] =  0; d[5][2] = -1;
  } else {
    // directions on the 2D lattice,  East,North,West,South
    // d[NA][DIM] = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};
    d[0][0] =  0; d[0][1] =  1;
    d[1][0] =  1; d[1][1] =  0;
    d[2][0] = -1; d[2][1] =  0;
    d[3][0] =  0; d[3][1] = -1;
  }

  // two monomer types
  pair_energy[0][0] = energy;  // H-H
  pair_energy[0][1] = 0;       // H-P
  pair_energy[1][0] = 0;       // P-H
  pair_energy[1][1] = 0;       // P-P

  cutoff = 1.005;
  cutsq = cutoff*cutoff;

  // Cv
  ntemp_bins = (int)((Tmax - Tmin)/dT) + 1;
  create(Cv, ntemp_bins);

  // interaction energy
  switch (seq) {
    case 1:
      mtypes = types1;
      break;
    case 2:
      mtypes = types2;
      break;
    case 3:
      mtypes = types3;
      break;
    case 4:
      mtypes = types4;
      break;
    default:    
      break;
  }

  if (seq == 0) {
    create(mtypes, L);
    for (int i = 0; i < L; i++) mtypes[i] = 0;
  }

  allocated_size = 2*dim*M0;
  allocated_size2 = M0;
  ntotal_bins = (int)((Ebmax - Ebmin) / dE) + 1;

  abs_diff = (int)(rel_diff*M0);

  // memory allocation
  // Enew, move and ind are approximately 2*dim*M0
  create(Enew, allocated_size);  // move: move[i][k] = move in direction k (= 0, 1, 2, 3) from parent i
  create(move, allocated_size, 2);
  create(ind, allocated_size);

  // polymer, NewPolymer and E are appoximately M0*dim*L
  create(polymer, allocated_size2, L, dim);
  create(NewPolymer, allocated_size2, L, dim);
  create(E, allocated_size2);

  create(gamma, L);
  create(gE, ntotal_bins);
  create(g, ntotal_bins);
  create(H, ntotal_bins);
  create(buffer, ntotal_bins);

  // initialize the seed for the random number generator for each MPI rank
  srand(seed+me);

  if (me == 0) {
    if (usinglog) {
      logfile = fopen(logfilename, "w");
      if (!logfile) {
        std::cerr << "Cannot open log file to write\n";
        usinglog = 0;
      }
    }

    // print out input parameters
    if (usinglog) {   
      fprintf(logfile, "MUCA Growth Algorithm for single chain on a %d-D lattice\n", dim);
      fprintf(logfile, "Target number of monomers:              %d\n", L);
      if (confinement_mode == CONFINEMENT_SPHERE)
        fprintf(logfile, "Confinement: inside sphere centered at (0,0,0), radius = %f\n", confinement.radius);
      else if (confinement_mode == CONFINEMENT_PLANE)
        fprintf(logfile, "Confinement: between 2 planes at z = %f and z = %f\n", confinement.zlo, confinement.zhi);
      else if (confinement_mode == CONFINEMENT_CYLINDER)
        fprintf(logfile, "Confinement: inside cylinder with axis (0,0,1), radius %f\n", confinement.radius);
      fprintf(logfile, "Number of replicas per MPI rank:        %d\n", M0);
      fprintf(logfile, "Number of MPI ranks:                    %d\n", nprocs);
      fprintf(logfile, "Initial memory allocated (MB):          %0.1f\n", memory_usage()/1048576.0);
      fprintf(logfile, "Absolute tolerance in population size:  %d\n", abs_diff);
      fprintf(logfile, "Random number generator seed:           %d\n", seed);
    }
    // print out input parameters
    printf("MUCA Growth Algorithm for single chain on a %d-D lattice\n", dim);
    printf("Target number of monomers:              %d\n", L);
    if (confinement_mode == CONFINEMENT_SPHERE)
        printf("Confinement: inside sphere centered at (0,0,0), radius = %f\n", confinement.radius);
      else if (confinement_mode == CONFINEMENT_PLANE)
        printf("Confinement: between 2 planes at z = %f and z = %f\n", confinement.zlo, confinement.zhi);
      else if (confinement_mode == CONFINEMENT_CYLINDER)
        printf("Confinement: inside cylinder with axis (0,0,1), radius %f\n", confinement.radius);
    printf("Number of replicas per MPI rank:        %d\n", M0);
    printf("Number of MPI ranks:                    %d\n", nprocs);
    printf("Initial memory allocated (MB):          %0.1f\n", memory_usage()/1048576.0);
    printf("Absolute tolerance in population size:  %d\n", abs_diff);
    printf("Random number generator seed:           %d\n", seed);
  }
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::setup()
{
  for (int i = 0; i < allocated_size2; i++)
    E[i] = Enew[i] = 0;

  for (int i = 0; i < allocated_size2; i++)
    for (int j = 0; j < L; j++)
      for (int k = 0; k < dim; k++)
        polymer[i][j][k] = 0;

  for (int i = 0; i < allocated_size2; i++)
    for (int j = 0; j < L; j++)
      for (int k = 0; k < dim; k++)
        NewPolymer[i][j][k] = 0;

  for (int i = 0; i < L; i++) gamma[i] = 1.0;

  // gE from Ebmin to Ebmax
  // initialize gE to 1 for all the bins
  for (int i = 0; i < ntotal_bins; i++) gE[i] = 0;
  //gE[ntotal_bins-1] = 1.0/dE;

  // initialize the values of Emin and Emax
  Emin = Emax = 0;

  Nbins = 1;

  // initialize the number of parents
  M = M0;
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::run()
{
  if (!_params_ready) return;

  if (num_runs > 1) multi_runs();
  else single_run();
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::single_run()
{
  init();

  double tstart, telapsed;
  tstart = MPI_Wtime();

  if (scheme == 0) ensemble_growth2();
  else ensemble_growth();

  telapsed = MPI_Wtime()-tstart;

  double tmp;
  MPI_Allreduce(&telapsed,&tmp,1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  compute_energy_histogram();

  if (me == 0) {
    if (usinglog) {
      fprintf(logfile, "\nAverage compute time of %0.1f seconds for %d monomers and %d replicas\n\n",
        telapsed/(double)nprocs, L, M0);
      fprintf(logfile, "Summary:\n");
      fprintf(logfile, "  - Number of configurations at the final stage = %d\n", M);
      fprintf(logfile, "  - Product of all the population control factors (P_gamma) = %e\n",
        acc_gamma);
      fprintf(logfile, "  - Estimated total number of all the configurations (1/P_gamma) = %e\n",
        1.0/acc_gamma);
      fflush(logfile);
    } 
    printf("\nAverage compute time of %0.1f seconds for %d monomers and %d replicas\n\n",
      (double)telapsed/nprocs, L, M0);
    printf("Summary:\n");
    printf("  - Number of configurations at the final stage = %d\n", M);
    printf("  - Product of all the population control factors (P_gamma) = %e\n",
      acc_gamma);
    printf("  - Estimated total number of all the configurations (1/P_gamma) = %e\n",
      1.0/acc_gamma);
    
    // compute the unweighted energy histogram 
    // and write out g(E) and C_v at the final step
    
    output_gE(L-1);
    compute_cv(L-1);

    if (write_final) write_dump();

    if (usinglog) {
      fprintf(logfile, "Output files:\n");
      fprintf(logfile, "  - Density of state and energy histogram: gE-%d-%dd.txt\n", L, dim);
      fprintf(logfile, "  - Specific heat: Cv-%d-%dd.txt\n", L, dim);
      fprintf(logfile, "\nMemory usage: %0.1f MB per MPI rank\n\n", memory_usage()/1048576.0);
      fclose(logfile);
    }
    printf("Output files:\n");
    printf("  - Density of state and energy histogram: gE-N%d-%dd.txt\n", L, dim);
    printf("  - Specific heat: Cv-%d-%dd.txt\n", L, dim);
    printf("\nMemory usage: %0.1f MB per MPI rank\n\n", memory_usage()/1048576.0);
  }

  //testing_compute_cv("exact-N26-3d.dat", Nbins, 0);
  //testing_compute_cv("exact-N43-2d.dat", Nbins, 0);
  //testing_compute_cv("exact-N40-2d.dat", Nbins, 0);
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::multi_runs()
{
  init();

  // sum_gE and sum_gEsq from Emin to Emax
  double *ave_gE = NULL;
  double *ave_gEsq = NULL;
  double *ave_Cv = NULL;
  double *ave_Cvsq = NULL;
  int* count = NULL;
  double* Mcount = NULL;
  create(ave_gE, ntotal_bins);
  create(ave_gEsq, ntotal_bins);
  create(ave_Cv, ntemp_bins);
  create(ave_Cvsq, ntemp_bins);
  create(count, ntotal_bins);
  create(Mcount, ntotal_bins);
  double sum_M = 0;
  double ave_inv_gamma = 0;
  for (int i = 0; i < ntotal_bins; i++) {
    ave_gE[i] = ave_gEsq[i] = 0;
    count[i] = 0;
    Mcount[i] = 0;
  }

  for (int i = 0; i < ntemp_bins; i++) {
    ave_Cv[i] = ave_Cvsq[i] = 0;
  }

  ofstream ofs;
  char filename[64];

  double tstart, telapsed;
  tstart = MPI_Wtime();

  // perform multiple runs
  for (int r = 0; r < num_runs; r++) {

    if (me == 0) {
      if (usinglog) {
        fprintf(logfile, "run = %d:\n", r);
        fflush(logfile);
      } 
      printf("run = %d:\n", r);
    }

    if (scheme == 0) ensemble_growth2();
    else ensemble_growth();

    compute_energy_histogram();

    // compute the unweighted energy histogram and write out g(E) and Cv
    
    output_gE(L-1, r);
    compute_cv(L-1, r);

    // accumulate to sum_gE and sum_gEsq    
    for (int i = 0; i < Nbins; i++) {
      if (H[i] > 0) {
        double E = Emin + i * dE;
        int ibin = (int)((E - Ebmin) / dE);
        ave_gE[ibin] += M*gE[i];
        ave_gEsq[ibin] += M*gE[i]*gE[i];
        count[ibin] += 1.0;
        Mcount[ibin] += M;
      }
    }

    for (int i = 0; i < ntemp_bins; i++) {
      ave_Cv[i] += M*Cv[i];
      ave_Cvsq[i] += M*Cv[i]*Cv[i];
    }

    if (debug_level == 1 && me == 0) {
      if (usinglog) {
        fprintf(logfile, "  population size, M       = %d\n", M);
        fflush(logfile);
      }
      printf("  population size, M       = %d\n", M);

      // write out g(E) for each run (debugging)
      sprintf(filename, "gE-%d-r-%d.txt", L, r);
      ofs.open(filename);
      for (int i = 0; i < Nbins; i++)
        ofs << Emin + i*dE << " " << gE[i] << " " << H[i] << "\n";
      ofs.close();     
    }

    // product of all the gamma's
    acc_gamma = 1.0;
    for (int i = 0; i < L; i++) acc_gamma *= gamma[i];
    ave_inv_gamma += 1.0/acc_gamma;

    sum_M += M;

    double sum_gE = 0;
    for (int i = 0; i < ntotal_bins; i++) {
      if (count[i] > 0) {
        sum_gE += exp(ave_gE[i]/Mcount[i]);
      }
    }

    if (me == 0) {
      sprintf(filename, "ave-gE-%d-%dd.txt", L, dim);
      ofs.open(filename);
      ofs << "E ave_gE stdev ave_gE/sum_gE count ; ";
      ofs << "sum = " << sum_gE << " 1/acc_gamma = " << ave_inv_gamma/(double)num_runs << "\n";
      for (int i = 0; i < ntotal_bins; i++) {
        if (count[i] > 0) {
          double running_ave_gE = ave_gE[i]/Mcount[i];
          double stdev = sqrt(ave_gEsq[i]/Mcount[i] - running_ave_gE*running_ave_gE)/(double)(r+1);
          ofs << Ebmin + i*dE << " " << running_ave_gE << " " << stdev << " ";
          ofs << exp(running_ave_gE)/sum_gE << " " << count[i] << "\n";
        }
      }
      ofs.close();

      sprintf(filename, "ave-Cv-%d-%dd.txt", L, dim);
      ofs.open(filename);
      ofs << "T C_v stdev\n";
      for (int i = 0; i < ntemp_bins; i++) {
        double running_ave_Cv = ave_Cv[i]/sum_M;
        double stdev = sqrt(ave_Cvsq[i]/sum_M - running_ave_Cv*running_ave_Cv)/(double)(r+1);
        ofs << Tmin + i*dT << " " << running_ave_Cv << " " << stdev << "\n";
        
      }
      ofs.close();
    }
  }

  telapsed = MPI_Wtime()-tstart;
  double tmp;
  MPI_Allreduce(&telapsed,&tmp,1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  // write out the final average g(E) and C_v

  double sum_gE = 0;
  for (int i = 0; i < ntotal_bins; i++) {
    if (count[i] > 0) {
      ave_gE[i] /= Mcount[i];
      sum_gE += exp(ave_gE[i]);
    }
  }

  if (me == 0) {
    sprintf(filename, "ave-gE-%d-%dd-final.txt", L, dim);
    ofs.open(filename);
    ofs << "E ave_gE stdev ave_gE/sum_gE count ; ";
    ofs << "sum = " << sum_gE << " 1/acc_gamma = " << ave_inv_gamma/(double)num_runs << "\n";
    for (int i = 0; i < ntotal_bins; i++) {
      if (count[i] > 0) {
        double stdev = sqrt(ave_gEsq[i]/Mcount[i] - ave_gE[i]*ave_gE[i])/(double)num_runs;
        ofs << Ebmin + i*dE << " " << ave_gE[i] << " " << stdev << " ";
        ofs << exp(ave_gE[i])/sum_gE << " " << count[i] << "\n";
      }
    }
    ofs.close();

    sprintf(filename, "ave-Cv-%d-%dd-final.txt", L, dim);
    ofs.open(filename);
    ofs << "T C_v stdev\n";
    for (int i = 0; i < ntemp_bins; i++) {
      ave_Cv[i] /= sum_M;
      double stdev = sqrt(ave_Cvsq[i]/sum_M - ave_Cv[i]*ave_Cv[i])/(double)num_runs;
      ofs << Tmin + i*dT << " " << ave_Cv[i] << " " << stdev << "\n";
      
    }
    ofs.close();
  }

  if (me == 0) {
    if (usinglog) {
      fprintf(logfile, "\nAverage compute time of %0.1f seconds for %d monomers and %d replicas for %d runs\n\n",
        telapsed/(double)nprocs, L, M0, num_runs);
      fprintf(logfile, "Output files:\n");
      fprintf(logfile, "  - Density of state and energy histogram: ave-gE-%d-%dd.txt\n", L, dim);
      fprintf(logfile, "  - Specific heat: ave-Cv-%d-%dd.txt\n", L, dim);
      fprintf(logfile, "\nMemory usage: %g MB per MPI rank\n\n", memory_usage()/1048576.0);
      fclose(logfile);
    }
    printf("\nAverage compute time of %0.1f seconds for %d monomers and %d replicas for %d runs\n\n",
      telapsed/(double)nprocs, L, M0, num_runs);
    printf("Output files:\n");
    printf("  - Density of state and energy histogram: ave-gE-%d-%dd.txt\n", L, dim);
    printf("  - Specific heat: ave-Cv-%d-%dd.txt\n", L, dim);
    printf("\nMemory usage: %g MB per MPI rank\n\n", memory_usage()/1048576.0);
  }

  destroy(ave_gE);
  destroy(ave_gEsq);
  destroy(ave_Cv);
  destroy(ave_Cvsq);
  destroy(count);
  destroy(Mcount);
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::ensemble_growth()
{
  setup();

  gamma[1] = 1.0/(double)NA;
  acc_gamma = 1.0;

  // Main Program
  // loop over the MC growth, (from n to n+1)

  for (int n = 1; n < L; n++) {

    // generate the daughters
    //   K0 = total number of daughters generated
    //   store Emin_parent and nbins_parent to look up ibin_parent later
    //   update Emin, Emax to the values of the daughters
    //   compute Nbins to the number of energy bins  for the daughters

    K0 = generate_daughters(n);

    if (debug_level == 2 && me == 0) {
      if (usinglog) {
        fprintf(logfile, "  no. of generated daughters K0 = %ld\n", K0);
        fflush(logfile);
      } else printf("  no. of generated daughters K0 = %ld\n", K0);
    }

    // at this point all self-avoiding daughter structures (K0) have been generated

    // Set initial values for gamma and g1
    // gamma[[n+1]]=1;
    gamma[n] = 1; //(double)M0 / (double)K0;

    // g1=g;
    for (int i = 0; i < ntotal_bins; i++) {
      //g[i] = gE[i];
 
      // map the energy bins from parents to daughters
      // get the energy in the daughter bins
      double e = Emin + i * dE;
      // find the corresponding bin in the parents' energy range
      int ibin_parent = (int)((e - Emin_parent)/dE);
      // check if the energy e is within the parents' energy range
      if (ibin_parent >= 0 && ibin_parent < nbins_parent) {
        g[i] = gE[ibin_parent];  // initialize g in this bin with gE of the parent
      } else {
        // if e is out of the parents' range, use the gE for lowest energy bin
        g[i] = gE[0];
      }

      if (debug_level == 4 && me == 0)
        printf("   i = %d; E = %f; g[i] = %0.16lf\n", i, Emin+i*dE, g[i]);
    }

    // Then select the daughters 
    // Daughters=SelectD[];
    // M[[n+1]]=Length[Daughters];
    if (fixed_size) {
      // estimate the number of daughters selected by the current values of gamma's
      // M can grow expenentially here
      double M_est = estimate_num_daughters(n);
      if (M_est <= 0) {
        if (me == 0)
          printf("ERROR: Overflow weights leading to invalid value of M = %d. "
            "Maybe increasing M0 would help.\n", M_est);
        MPI_Finalize();
      }
      
      // rescale gamma based on the estimated M
      gamma[n] = gamma[n] * (double)M0 / M_est;
      // now actually select the daughters based on the rescaled gamma's
      M = select_daughters(n);

      while (fabs(M - M0) > abs_diff) {
        // rescale gamma
        gamma[n] =  gamma[n] * (double)M0 / (double)M;
        // select daughter again
        M = select_daughters(n);
      }

    } else {
      M = K0;
      for (uint64_t k = 0; k < K0; k++) ind[k] = k;
    }


    // Compute the population number and the histogram 
    // EnergyD=Daughters[[All,3]];
    // H=density[EnergyD];
    compute_energy_histogram();
   
    // compute the mean value of the energy histogram
    // meanH=Total[H]/(Total[Unitize[H]]);
    int nonzero_bins = 0; 
    int sumH = 0;
    for (int i = 0; i < Nbins; i++) {
      if (H[i] > 0) {
        sumH += H[i];
        nonzero_bins++;
      }
    }
    double meanH = (double)sumH / (double)nonzero_bins;  

    // rescale once
    //gamma[[n+1]] = gamma[[n+1]] *M0 / M[[n+1]];
    //g1=H/meanH*g1;

    //gamma[n] = gamma[n] * (double)M0 / (double)M;  // no need to rescale gamma here as already iterated above
    for (int i = 0; i < Nbins; i++) {
      //if (H[i] > 0) g[i] = H[i] / meanH * g[i];
      if (H[i] > 0) g[i] = log(H[i] / meanH) + g[i]; // now g is actually log(g)
      if (debug_level == 4 && me == 0)
        printf("   i = %d: E = %f; g[i] = %0.16lf: H[i] = %d; meanH = %f; gamma = %f, M = %d\n",
           i, Emin+i*dE, g[i], H[i], meanH, gamma[n], M);
    }

    // Sample once more with the rescaled values
    // Daughters=SelectD[];
    if (fixed_size) {
      
      M = select_daughters(n);
      
      // compute the energy histogram from the selected daughters
      // EnergyD=Daughters[[All,3]];
      // H=density[EnergyD];  
      compute_energy_histogram();

      // check if H is flat
      double d_KL = 0;
      int flat = check_flatness(H, Nbins, flat_tolerance, d_KL);
      if (debug_level == 4 && me == 0) {
        printf("  n = %d flat check: d_KL = %f, flat = %d; gamma = %f; M = %d\n",
          n, d_KL, flat, gamma[n], M);
      }

      // improve gamma a little bit more
      /*
      if (!flat) {
        gamma[n] = gamma[n] * (double)M0 / (double)M;
        M = select_daughters(n);
      }
      */
    }

    // Let's update everything n-->n+1
    // apply the moves to the daughters
    // store the daughters into the parent for the next growth stage
    // update gE with g

    update_configurations(n);

    acc_gamma *= gamma[n];

    if ((n + 1) % log_every == 0 || n == L-1) {
      if (me == 0) {
        if (usinglog) {
          fprintf(logfile, "N = %4d : M = %8d, Emin = %g; Emax = %g\n", n+1, M, Emin, Emax);
          fflush(logfile);
        }
        printf("N = %4d : M = %8d, Emin = %g; Emax = %g\n", n+1, M, Emin, Emax);
      }
    }

    if (write_every) {
      if ((n + 1) % write_every == 0) {
        output_gE(n);
        compute_cv(n);
      }
    }
  } // end the loop on the growth
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::ensemble_growth2()
{
  setup();

  gamma[0] = 1.0;
  acc_gamma = 0.0; // log(\prod^n_i gamma_i)

  // Main Program
  // loop over the MC growth, (from n to n+1)

  double sigma_last = 1.0;
  double sigma = sigma_last;

  for (int n = 1; n < L; n++) {

    // g1=1;
    for (int i = 0; i < ntotal_bins; i++) {
      g[i] = 0;
    }

    // generate the daughters
    //   K0 = total number of daughters generated (LD)
    //   store Emin_parent and nbins_parent to look up ibin_parent later
    //   update Emin, Emax to the values of the daughters
    //   compute Nbins to the number of energy bins  for the daughters

    K0 = generate_daughters(n);

    if (debug_level == 2 && me == 0) {
      if (usinglog) {
        fprintf(logfile, "  no. of generated daughters K0 = %ld\n", K0);
        fflush(logfile);
      } else printf("  no. of generated daughters K0 = %ld\n", K0);
    }

    // at this point all self-avoiding daughter structures (K0) have been generated

    // Compute the weighted energy histogram
    // each daugher with her initial weight being gE[parent]

    compute_weighted_energy_histogram(n);
   
    // compute the support of the energy histogram (number of non-zero bins)

    int nonzero_bins = 0; 
    for (int i = 0; i < Nbins; i++) {
      if (H[i] > 0) nonzero_bins++;
    }
    sigma = nonzero_bins;

    // compute the population gamma from the supports
    if (fixed_size) {
      
      // compute the new population gamma from the ratio of the supports: sigma[n-1]/sigma[n]
      gamma[n] = (double)sigma_last/sigma;

      if (debug_level == 4 && me == 0)
        printf("n = %d: gamma = %f; sigma = %f; sigma_last = %f\n", n, gamma[n], sigma, sigma_last);
      
    } else {
      M = K0;
      for (uint64_t k = 0; k < K0; k++) ind[k] = k;
    }

    // compute g[i] from the weighted histogram H[i]
    for (int i = 0; i < Nbins; i++) {
      
      if (H[i] > 0) g[i] = log(H[i]) - log(M0) - acc_gamma; // now g is actually log(g)

      if (debug_level == 4 && me == 0)
        printf("   i = %d: E = %f; g[i] = %0.16lf: H[i] = %d; gamma = %f, M = %d\n",
           i, Emin+i*dE, g[i], H[i], gamma[n], M);
    }

    // now actually select the daughters based on gamma[n] and sigma[n]
    M = select_daughters(n);
    
    // Let's update everything n-->n+1
    // apply the moves to the daughters
    // store the daughters into the parent for the next growth stage
    // update gE with g

    update_configurations(n);

    // update the support of the energy histogram

    sigma_last = sigma;
    acc_gamma += log(gamma[n]);

    if ((n + 1) % log_every == 0 || n == L-1) {
      if (me == 0) {
        if (usinglog) {
          fprintf(logfile, "N = %4d : M = %8d, Emin = %g; Emax = %g\n", n+1, M, Emin, Emax);
          fflush(logfile);
        }
        printf("N = %4d : M = %8d, Emin = %g; Emax = %g\n", n+1, M, Emin, Emax);
      }
    }

    if (write_every) {
      if ((n + 1) % write_every == 0) {
        output_gE(n);
        compute_cv(n);
      }
    }
  } // end the loop on the growth

  acc_gamma = exp(acc_gamma);
}

/*---------------------------------------------------------------------------
  update the unweighted energy histogram H(E) from 
    M    the number of the selected daughters,
    Enew the energies of all the generated daughters, and
    ind  the indices of the selected daughters in the all daughters array
  Note: Graziano divided H(E) by dE (dE = 1 in lattice models here)
-----------------------------------------------------------------------------*/

void MUCAGrowth::compute_energy_histogram()
{
  for (int i = 0; i < ntotal_bins; i++) H[i] = buffer[i] = 0;

  for (int i = 0; i < M; i++) {
    // find the energy of the selected daughter
    double Ei = Enew[ind[i]];
    // find the energy bin corresponding to the daughter relative to Emin
    int ibin = (int)((Ei - Emin) / dE);
    // update the number of configurations in the bin
    buffer[ibin] = buffer[ibin] + 1;
  }

  MPI_Allreduce(&buffer[0], &H[0], ntotal_bins, MPI_DOUBLE, MPI_SUM,
     MPI_COMM_WORLD);
}

/*---------------------------------------------------------------------------
  compute the energy histogram H(E) with weights from ALL the generated daughters
-----------------------------------------------------------------------------*/

void MUCAGrowth::compute_weighted_energy_histogram(int n)
{
  for (int i = 0; i < ntotal_bins; i++) H[i] = buffer[i] = 0;

  for (int k = 0; k < K0; k++) {
    // find the energy of the selected daughter
    double Ei = Enew[k];
    // find the energy bin corresponding to the daughter relative to Emin
    int ibin = (int)((Ei - Emin) / dE);

    // find the energy bin corresponding to the parent
    int parent = move[k][0];
    int ibin_parent = (int)((E[parent] - Emin_parent) / dE);
    // update the number of configurations in the bin with the weights
    double w = exp(gE[ibin_parent]);
    buffer[ibin] = buffer[ibin] + w;
  }

  MPI_Allreduce(&buffer[0], &H[0], ntotal_bins, MPI_DOUBLE, MPI_SUM,
     MPI_COMM_WORLD);
}

/*---------------------------------------------------------------------------
  generate all the daughter configurations at step n
  for each sample of each parent configuration, grow it by adding a monomer
     store the move, its energy but not the entire new configuration
  return total number of daughters generated
         store Emin_parent and nbins_parent to look up ibin_parent later
         update Emin, Emax to the values of the daughters
         compute Nbins to the number of energy bins  for the daughters
-----------------------------------------------------------------------------*/

uint64_t MUCAGrowth::generate_daughters(int n)
{
  // type of the new monomer

  int ntype = mtypes[n];

  // loop over all the current configurations
  //   ticket dispenser, to label all the new daughter structures
  uint64_t ticket = 0;
  for (int i = 0; i < M; i++) {

    // get the coordinates of the last monomer to grow from
    double x[3];
    for (int ii = 0; ii < dim; ii++)
        x[ii] = polymer[i][n-1][ii];

    // create daughter configurations (try all NA possibilities )
    for (int dir = 0; dir < NA; dir++) {

      // new monomer
      double t[3];
      for (int ii = 0; ii < dim; ii++)
        t[ii] = x[ii] + d[dir][ii];

      // compute the energy (and check self-avoidance)
      double DeltaE = 0;
      int SelfIntersect = 0;
      int outsideConfinement = 0;

      // check for self-avoidance; exclude bonded (consecutive) monomers
      for (int j = 0; j <= n - 1; j++) {
        double eng, del[3];
        double rsq = 0;
        for (int ii = 0; ii < dim; ii++) {
          del[ii] = polymer[i][j][ii] - t[ii];
          rsq += del[ii]*del[ii];
        }

        int jtype = mtypes[j];

        // check for self-intersecting
        if (rsq < EPSILON)  {
          SelfIntersect = 1;
          break;
        }

        // apply confinement
        eng = 0;
        outsideConfinement = apply_confinement(t, eng, n);
        if (outsideConfinement) break;

        // non-bonded nearest neighbor && j != n-1
        if (rsq < cutsq && j != n-1) {
          eng = compute_energy(x, ntype, jtype, rsq);
          DeltaE = DeltaE + eng; //pair_energy[ntype][jtype];
        }
      }

      if (SelfIntersect == 0 && outsideConfinement == 0) {
        // memorize the move
        // the daughter is identified by just the parent and direction
        move[ticket][0] = i;
        move[ticket][1] = dir; 
        Enew[ticket] = E[i] + DeltaE;
        ticket = ticket + 1;

        // grow the arrays if needed
        if (ticket == allocated_size) {
          if (debug_level == 3 && me == 0) {
            if (usinglog) {
              fprintf(logfile, "generating daughters: grow arrays from size %ld at n = %d\n",
                allocated_size, n);  
              fflush(logfile);
            } else printf("generating daughters: grow arrays from size %ld at n = %d\n",
                    allocated_size, n);
          }
          allocated_size += DELTA;
          //grow_arrays(allocated_size);
          grow(Enew, allocated_size);
          grow(move, allocated_size, 2);
          grow(ind, allocated_size);
        }
      }
    } // end of loop through directions
  } // end of all current configurations

  // K0 is the number of all the generated self-avoiding daughters
  K0 = ticket;

  // save the Emin of the parents and the number of bins
  // at this point gE is of the parents
  Emin_parent = Emin;
  double Emax_parent = Emax;
  nbins_parent = (int)((Emax - Emin)/dE) + 1;

  // find the Emin and Emax from all the daughter configurations
  double emin_local = Enew[0];
  double emax_local = Enew[0];
  for (uint64_t i = 0; i < K0; i++) {
    if (emin_local > Enew[i]) emin_local = Enew[i];
    if (emax_local < Enew[i]) emax_local = Enew[i];
  }
  MPI_Allreduce(&emin_local, &Emin, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(&emax_local, &Emax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  if (Emax < Emax_parent) Emax = Emax_parent;

  // update the number of bins from Emin to Emax
  Nbins = (int)((Emax - Emin) / dE) + 1;

  return K0;
}

/*---------------------------------------------------------------------------*/

double MUCAGrowth::estimate_num_daughters(int n)
{
  double ticket = 0;

  for (uint64_t k = 0; k < K0; k++) {
    // find the energy bin corresponding to the daughter k
    int ibin = (int)((Enew[k] - Emin) / dE);
    // find the energy bin corresponding to the parent
    int parent = move[k][0];
    int ibin_parent = (int)((E[parent] - Emin_parent) / dE);

    // compute the replication weight
    //double w = gE[ibin_parent] / g[ibin] * gamma[n];
    double w = exp(gE[ibin_parent] - g[ibin]) * gamma[n]; // g and gE are actually log(g) and log[g(E)]

    if (w > M0) {
      if (me == 0 && debug_level == 3)
        printf("WARNING: Too big weight w = %0.1f: dlog[g(E)] = %0.1f; "
          "ibin_parent = %d; E_parent = %f; ibin = %d; E_daughter = %f; "
          "M = %d; allocated_size = %d; ticket = %f\n",
          w, gE[ibin_parent] - g[ibin], ibin_parent, E[parent], ibin, Enew[k],
          M, allocated_size, ticket);
    }
    ticket += w;
  }

  return ticket;
}

/*---------------------------------------------------------------------------*/

uint64_t MUCAGrowth::select_daughters(int n)
{
  uint64_t ticket = 0;

  for (uint64_t k = 0; k < K0; k++) {
    // find the energy bin corresponding to the daughter k
    int ibin = (int)((Enew[k] - Emin) / dE);
    // find the energy bin corresponding to the parent
    int parent = move[k][0];
    int ibin_parent = (int)((E[parent] - Emin_parent) / dE);

    // compute the replication weight
    //double w = gE[ibin_parent] / g[ibin] * gamma[n];
    double w = exp(gE[ibin_parent] - g[ibin]) * gamma[n]; // g and gE are actually log(g) and log[g(E)]
    int64_t r = floor(w);
    if ((double)rand()/(double)RAND_MAX < (w - r)) r += 1;
/*
    
    if (ticket + r >= allocated_size) {
      if (debug_level == 3 && me == 0) {
        if (usinglog) {
          fprintf(logfile, "selecting daughters: grow ind from size %ld at n = %d\n",
            allocated_size, n);  
          fflush(logfile);
        } else printf("selecting daughters: grow ind from size %ld at n = %d\n",
            allocated_size, n);
      }
      while (allocated_size < ticket + r) allocated_size += DELTA;
      printf("grow ind for selecting daughters at n = %d: allocated_size = %d\n", n, allocated_size);
      grow(ind, allocated_size);
    }
*/
    // replicate the daughter k by r times

    for (int j = 0; j < r; j++) {
      ind[ticket] = k;
      ticket = ticket + 1;      
    }
  }

  return ticket;
}

/*---------------------------------------------------------------------------
   Compute potential energy of the added monomer n located at x of type ntype
     x is needed to check with the boundary condition (if applicable)
     jtype is the type of the non-bonded neighbor j within cutsq
     rsq is the squared distance between two monomers n and j
  -----------------------------------------------------------------------------*/

double MUCAGrowth::compute_energy(double* x, int ntype, int jtype, double rsq)
{
  return pair_energy[ntype][jtype];
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::update_configurations(int n)
{
  if (allocated_size2 <= M) {
    while (allocated_size2 <= M) allocated_size2 += DELTA;
    grow(polymer, allocated_size2, L, dim);
    grow(NewPolymer, allocated_size2, L, dim);
    grow(E, allocated_size2);
  }
  
  // apply the moves to the selected daughters -> NewPolymer

  for (int k = 0; k < M; k++) {
    int idx = ind[k];
    int parent = move[idx][0];
    int dir = move[idx][1];
    //NewPolymer(k,:,:)=polymer(move(ind(k),1),:,:);
    for (int l = 0; l < n; l++) {
      for (int ii = 0 ; ii < dim; ii++)
        NewPolymer[k][l][ii] = polymer[parent][l][ii];
    }

    for (int ii = 0 ; ii < dim; ii++)
      NewPolymer[k][n][ii] = polymer[parent][n-1][ii] + d[dir][ii];
  }

  // polymer = NewPolymer;
  for (int k = 0; k < M; k++) {
    for (int l = 0; l <= n; l++) {
      for (int ii = 0 ; ii < dim; ii++)
        polymer[k][l][ii] = NewPolymer[k][l][ii];
    }
  }

  // E from E0 and ind
  for (int i = 0; i < M; i++)
    E[i] = Enew[ind[i]];

  // update gE with the new g
  double gmax = g[0];
  for (int i = 0; i < Nbins; i++) {
    if (H[i] > 0) {
      if (gmax < g[i]) gmax = g[i];
    }
  }
    
  for (int i = 0; i < Nbins; i++) {
    gE[i] = g[i] - gmax;
  }
}


/*---------------------------------------------------------------------------
   apply external field (and constraints)
-----------------------------------------------------------------------------*/

int MUCAGrowth::apply_confinement(double* t, double& eng, int n)
{
  int outside = 0;
  double rsq = t[0]*t[0] + t[1]*t[1] + t[2]*t[2];
  double rxysq = t[0]*t[0] + t[1]*t[1];
  double R2 = confinement.radius*confinement.radius;

  if (confinement_mode == CONFINEMENT_SPHERE) {
    if (rsq >= R2) outside = 1;

  } else if (confinement_mode == CONFINEMENT_PLANE) {
    if (t[2] <= confinement.zlo || t[2] >= confinement.zhi) outside = 1;

  } else if (confinement_mode == CONFINEMENT_CYLINDER) {
    if (rsq >= R2) outside = 1;

  }

  return outside;
}

/*---------------------------------------------------------------------------
   Check for histogram flatness based on Kullbach-Leibler
-----------------------------------------------------------------------------*/

int MUCAGrowth::check_flatness(double* histogram, int nbins, double tolerance,
  double& d_KL)
{
  int i, m, flat = 0;
  
  double stdev = 0.0;

  // Nbins = length(find(H));
  // NEvents = sum(H);
  int nonzero_bins = 0; 
  double sumH = 0;
  for (i = 0; i < nbins; i++) {
    if (histogram[i] > 0) nonzero_bins++;
    sumH += histogram[i];
  }

  d_KL = 0;
  for (i = 0; i < nbins; i++) {
    if (histogram[i] > 0) {
      double Pi = histogram[i] / sumH;
      double Qi = 1.0 / (double)nonzero_bins;
      d_KL = d_KL + Pi*log(Pi/Qi);
    }
  }

  // is flat?
  if (d_KL < tolerance) flat = 1;
  return flat;
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::grow_arrays(int new_size)
{
  grow(polymer, new_size, L, dim);
  grow(NewPolymer, new_size, L, dim);
  grow(E, new_size);
  grow(Enew, new_size);
  grow(move, new_size, 2);
  grow(ind, new_size);
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::output_gE(int n, int irun)
{
  
  ofstream ofs;
  char filename[64];
  if (irun == 0) sprintf(filename, "gE-%d-%dd.txt", n+1, dim);
  else sprintf(filename, "gE-%d-%dd-run-%d.txt", n+1, dim, irun);

  double sum_gE = 0;
  for (int i = 0; i < Nbins; i++)
    if (H[i] > 0) sum_gE += exp(gE[i]);
  
  char* line = new char [256];
  
  ofs.open(filename);
  ofs << "E log[g(E)] gE/sum_gE H(E); sum_gE = " << sum_gE << " 1/acc_gamma = " << (1.0/acc_gamma) << "\n";
  // E g(E) g(E)/sum_gE H(E)
  for (int i = 0; i < Nbins; i++) {
    if (H[i] > 0) {
      sprintf(line, "%g %0.16e %0.16e %g\n", Emin + i*dE,
         gE[i], exp(gE[i])/sum_gE, H[i]);
      ofs << line;
    }
  }

  ofs.close();
  
  delete [] line;
  
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::compute_cv(int n, int irun)
{
  int i, start_bin=0;
  double T = Tmin;
  double* F = new double [Nbins];
  double* var = new double [Nbins];

  ofstream ofs;
  char filename[128];
  if (irun == 0) sprintf(filename, "Cv-%d-%dd.txt", n+1, dim);
  else sprintf(filename, "Cv-%d-%dd-run-%d.txt", n+1, dim, irun);
  ofs.open(filename);
  ofs << "T C_v\n";
  for (int m = 0; m < ntemp_bins; m++) {
    double T = Tmin + m * dT;
    double beta = 1.0/T;
    double Fmax=0;
    int first = 1;

    // find the maximum free energy in the energy spectrum:
    // beta F = log[g(E)] - beta E (i.e. F/T = S - E/T)
    // note that now g is actually log(g)
    for (i = start_bin; i < Nbins; i++) {
      if (H[i] > 0) {
        double Ei = Emin + i * dE;
        F[i] = -beta*Ei + g[i];
        if (first) {
          first = 0;
          Fmax = F[i];
        }
        if (Fmax < F[i]) Fmax = F[i];
      }
    }

    // subtract all the free energies from the maximum value
    // get the exponent exp(F)
    double X_MAX = -log(DBL_EPSILON);
    for (i = start_bin; i < Nbins; i++) {
      if (H[i] > 0) {
        double Ei = Emin + i * dE;
        F[i] = (-beta*Ei + g[i]) - Fmax;
        F[i] = exp(F[i]);
      } else {
        F[i] = 1;
      }
    }
    // Graziano's code: trapezoidal integration
    
    double Z = (F[0] + F[Nbins-1])*dE/2;
    for (i = 1; i < Nbins-1; i++) {
      if (H[i] > 0) Z += (F[i] + F[i+1])*dE/2;
    }

    double ave_E = (Emin*F[0] + Emax*F[Nbins-1])*dE/2;
    for (i = 1; i < Nbins-1; i++) {
      if (H[i] > 0) {
        double E1 = Emin + i * dE;
        double E2 = Emin + (i+1) * dE;
        ave_E += (E1*F[i] + E2*F[i+1])*dE/2;
      }
    }
    ave_E = ave_E/Z;

    for (i = 0; i < Nbins; i++) {
      if (H[i] > 0) {
        double Ei = Emin + i * dE;
        var[i] = (Ei - ave_E)*(Ei - ave_E);
      } else var[i] = 0;
    }

    double ave_E2 = (var[0]*F[0] + var[Nbins-1]*F[Nbins-1])*dE/2;
    for (i = 1; i < Nbins-1; i++) {
      if (H[i] > 0) ave_E2 += (var[i]*F[i] + var[i]*F[i+1])*dE/2;
    }
    Cv[m] = beta*beta * ave_E2/Z;

    // Trung's code
/*
    double Z = 0;
    for (i = 0; i < Nbins; i++) {
      if (H[i] > 0) Z += F[i];
    }

    double ave_E = 0;
    double ave_E2 = 0;
    for (i = 0; i < Nbins; i++) {
      if (H[i] > 0) {
        double Ei = Emin + i * dE;
        ave_E  += Ei*F[i];
        ave_E2 += Ei*Ei*F[i];
      }
    }
    ave_E = ave_E/Z;
    ave_E2 = ave_E2/Z;
    Cv[m] = (beta*beta)*(ave_E2 - ave_E*ave_E);
*/
    ofs << T << " " << Cv[m]/(double)L << "\n";

    T += dT;
  }

  delete [] var;
  delete [] F;
  ofs.close();
}

/*---------------------------------------------------------------------------*/

double MUCAGrowth::memory_usage()
{
  double bytes = 0;
  bytes += allocated_size2 * L * dim * sizeof(double); // polymer
  bytes += allocated_size2 * L * dim * sizeof(double); // NewPolymer
  bytes += allocated_size2 * L * dim * sizeof(double); // E
  bytes += allocated_size * sizeof(double);            // Enew
  bytes += allocated_size * 2 * sizeof(int);           // move
  bytes += allocated_size * sizeof(int);               // ind
  return bytes;
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::write_dump()
{
  int count = 0;
  double Emin_in_M = E[0];
  for (int k = 0; k < M; k++)
    if (E[k] < Emin_in_M) Emin_in_M = E[k];
  int ibin_min =  (int)((Emin_in_M - Emin) / dE);

  for (int k = 0; k < M; k++) {
    int ibin = (int)((E[k] - Emin) / dE);
    if (ibin != ibin_min) continue;
    count++;
  }

  int id = 1;
  char filename[256];
  sprintf(filename, "final-%d.dump", L);
  ofstream ofs;
  ofs.open(filename);
  ofs << "ITEM: TIMESTEP\n";
  ofs << "0\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << count * L << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << -2*L << " " << 2*L << "\n";
  ofs << -2*L << " " << 2*L << "\n";
  ofs << -2*L << " " << 2*L << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z\n"; // q stores energy of the chains

  int mol = 1;
  for (int k = 0; k < M; k++) {
    int ibin = (int)((E[k] - Emin) / dE);
    if (ibin != ibin_min) continue; // only write out lowest energy configurations
    for (int m = 0; m < L; m++) {
      ofs << id << " " << mol << " " << mtypes[m]+1 << " " << E[k] << " ";
      for (int ii = 0 ; ii < dim; ii++)
        ofs << polymer[k][m][ii] << " ";
      ofs << "\n";
      id++;
    }
    mol++;
  }
  ofs.close();
}

/*---------------------------------------------------------------------------*/

void MUCAGrowth::testing_compute_cv(const char* filename, int nbins, int start_bin)
{
  Emin = -nbins+1;
  Emax = 0;
  Nbins = (int)((Emax - Emin)/dE) + 1;
  printf("Testing with exact enumeration Nbins = %d\n", Nbins);

  char* line = new char [256];
  ifstream ifs;
  ifs.open(filename);
  ifs.getline(line, 256);
  double *g_exact = new double [nbins];
  int *h = new int [nbins];
  for (int i = 0; i < nbins; i++) {
    ifs.getline(line, 256);
    int e;
    double normalized_g;
    sscanf(line, "%d %lf %lf %d",
       &e, &g_exact[i], &normalized_g, &h[i]);
  }
  ifs.close();
  
  int i;
  double T = Tmin;
  double* F = new double [Nbins];
  double* var = new double [Nbins];

  ofstream ofs;
  ofs.open("Cv-test.txt");

  for (int m = 0; m < ntemp_bins; m++) {
    double T = Tmin + m * dT;
    double beta = 1.0/T;
    double Fmax=0;
    int first = 1;

    // find the maximum free energy in the energy spectrum:
    // beta F = log[g(E)] - beta E (i.e. F/T = S - E/T)
    for (i = start_bin; i < Nbins; i++) {
      if (h[i] > 0) {
        double Ei = Emin + i * dE;
        F[i] = -beta*Ei + log(g_exact[i]);
        if (first) {
          first = 0;
          Fmax = F[i];
        }
        if (Fmax < F[i]) Fmax = F[i];
      }
    }
    // subtract all the free energies from the maximum value
    // get the exponent exp(F)
    for (i = start_bin; i < Nbins; i++) {
      if (h[i] > 0) {
        double Ei = Emin + i * dE;
        F[i] = (-beta*Ei + log(g_exact[i])) - Fmax;
        F[i] = exp(F[i]);
      } else {
        F[i] = 1;
      }
    }

    // Graziano's code: trapezoidal integration
    
    double Z = (F[start_bin] + F[Nbins-1])*dE/2;
    for (i = start_bin+1; i < Nbins-1; i++)
      if (h[i] > 0)  Z += (F[i] + F[i+1])*dE/2;

    double ave_E = (Emin*F[0] + Emax*F[Nbins-1])*dE/2;
    for (i = start_bin+1; i < Nbins-1; i++) {
      if (h[i] > 0) {
        double E1 = Emin + i * dE;
        double E2 = Emin + (i+1) * dE;
        ave_E += (E1*F[i] + E2*F[i+1])*dE/2;
      }
    }
    ave_E = ave_E/Z;


    for (i = start_bin; i < Nbins; i++) {
      if (h[i] > 0) {
        double Ei = Emin + i * dE;
        var[i] = (Ei - ave_E)*(Ei - ave_E);
      } else var[i] = 0;
    }

    double ave_E2 = (var[0]*F[0] + var[Nbins-1]*F[Nbins-1])*dE/2;
    for (i = start_bin+1; i < Nbins-1; i++)
      if (h[i] > 0) ave_E2 += (var[i]*F[i] + var[i]*F[i+1])*dE/2;

    double cv = beta*beta * ave_E2/Z;

    // Trung's code
/*    
    double Z = 0;
    for (i = start_bin; i < Nbins; i++) 
      if (h[i] > 0) Z += F[i];
    
    double ave_E = 0;
    double ave_E2 = 0;
    for (i = start_bin; i < Nbins; i++) {
      if (h[i] > 0) {
        double Ei = Emin + i * dE;
        ave_E  += Ei*F[i];
        ave_E2 += Ei*Ei*F[i];
      }
    }
    ave_E = ave_E/Z;
    ave_E2 = ave_E2/Z;

    double cv = (beta*beta)*(ave_E2 - ave_E*ave_E);
*/    
    ofs << T << " " << cv << "\n";

  }

  delete [] g_exact;
  delete [] h;
  delete [] line;

  delete [] var;
  delete [] F;
  ofs.close();
}
