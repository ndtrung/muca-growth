/*
 Multicanonical Growth, last update Auguest 1, 2018
 adapted from Orland & Garel 1990
 from discussions of H.Orland, G.Vernizzi (Saclay 5-9/2017)
 implementation by T.Nguyen & G.Vernizzi  (NU 8-10/2017)
 supported by M. Olvera de la Cruz (while at Northwestern, 7-12/2017)
 supported by H. Orland (while at Saclay, 5-9/2017)

 Case studies: A single HP chain on a square (2-D) or cubic (3-D) lattice
             test with 5 sequences where sequence 0 corresponds to a homopolymer
             sequences 1-4 are two-letter copolymers consisting of up to 14-mer 
             pair energy H-H = 0 corresponding to a self-avoiding walk chain

 
  14 0: HHHH HHHH HHHH HH
  14.1: HPHP HHPH PHHP PH
  14.2: HHPP HPHP HHPH PH
  14.3: HHPH PHPP HPHP HH
  14.4: HHPH PPHP HPHH PH
    H => type 0
    P => type 1

 Use: ./run_muca num_monomers L replicas M seed [int] num_runs [int] sequence [int] write_final [0/1] write_every [0/num]
   L = number of monomers
   M = number of replicas to use
   num_runs = number of runs to perform for averaging g(E)
   sequence = index of the sequence, can be 0, 1, 2, 3 or 4 (can be added more)
   write_final = 1 = write out the configurations when the whole length is reached, 0 not writing out

 Run: ./run_muca dimension 2 replicas 10000 seed 2948112 num_runs 10 sequence 1

   for chain length of L = 14 monomers with M0 = 10000 copies,
   for sequence 1, random seed 2948112, perform 10 runs with shifted seeds
  
 Output:
   ave-SE.txt
     with 4 columns: E, ln[g(E)], stdev, no. of runs that sample configurations in that bin

 Ref: MacDonald, et al.,Self-avoiding walks on the simple cubic lattice,
        J. Phys. A: Math. Gen. 2000, 33 5973

 Ref: Bachmann and Janke, Density of states for HP lattice proteins,
   Acta Physica Polonica B 34, 4689-4697 (2003)

 Note: Vogel, Bachmann, Janke, Freezing and collapse of flexible polymers
   on regular lattices in three dimensions, Phys. Rev. E (2007) 76:061803.
   interaction between neighboring sites: epsilon = -1
   Specific heat as function of T depends on the chain length N:
     T > T_theta: random coil phase (dissolved, unstructured)
     T_m < T < T_theta: globular phase (liquid-like, unstructured)
     T < T_m: locally crystalline or amorphous metastable structures (solid-like), T_m = melting point

*/

#include <mpi.h>
#include "muca_growth.h"

using namespace MC;
using namespace std;

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);

  MUCAGrowth muca(argc, argv);
  muca.run();

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  return 0;
}
