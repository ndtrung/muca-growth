Implemetation of the multicanonical Monte Carlo growth method as described in
G. Vernizzi, T. D. Nguyen, H. Orland, M. Olvera de la Cruz,
Multicanonical Monte Carlo Ensemble Growth Algorithm, arXiv:1811.00349

Compute the density of states g(E) and heat capacity C_v of a polymer chain on simple lattices.

* Build

make

If you don't have a C++ MPI compiler, edit Makefile to change from mpic++ to your C++ compiler, for example, g++.

* Examples

a) Interacting self-avoiding walk (ISAW) chain up to 128 monomers on a simple cubic lattice using 1000000 replicas per MPI rank,
write out the densities of states every 10 steps :

mpirun -np 4 ./run_muca num_monomers 128 dimension 3 replicas 1000000 write_every 10 log log.txt

b) Interacting self-avoiding walk (ISAW) chain up to 64 monomers on a simple square lattice using 100000 replicas per MPI rank:
write out the densities of states every 2 steps :

mpirun -np 16 ./run_muca num_monomers 64 dimension 2 replicas 100000 write_every 2 log log.txt





