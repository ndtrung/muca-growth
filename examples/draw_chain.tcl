proc visualize {molid len} { 
  lassign [molinfo top get "{selection 0}"] select_text
  set select_atoms [atomselect top $select_text]
  set natoms  [$select_atoms num]


  set res 32
  set startidx 0
  draw_chain $molid $startidx $len $res

}

# types 1-2 hydrophobic
# types 3-4 hydrophilic
proc draw_chain {molid startidx len res} {

  set nbonds [expr $len - 1]
  set nbondsm1 [expr $nbonds - 1]
	for {set i 0} {$i < $nbonds} {incr i}  {

    set atom_index1 [expr $startidx + $i]
  	set atom_index2 [expr $startidx + $i + 1]
  	set select_tip1 [atomselect $molid "index $atom_index1"]
  	set select_tip2 [atomselect $molid "index $atom_index2"]

  	set coord1 [lindex [$select_tip1 get {x y z}] 0]
  	set coord2 [lindex [$select_tip2 get {x y z}] 0]
    set type1 [lindex [$select_tip1 get {type}] 0]
    set type2 [lindex [$select_tip2 get {type}] 0]

   	graphics $molid color white
  	graphics $molid cylinder $coord1 $coord2 radius 0.15 resolution $res filled yes

    # orange red
    if {$type1 == 0} {
    	graphics $molid color orange
    }
    if {$type1 == 1} {
    	graphics $molid color blue2
    }

  	draw sphere $coord1 radius 0.3 resolution $res

    if {$i == $nbondsm1} {
      if {$type2 == 0} {
      	graphics $molid color orange
      }
      if {$type2 == 1} {
      	graphics $molid color blue2
      }

    	draw sphere $coord2 radius 0.3 resolution $res	
    }
  }

}

# Required: num_monomers chainIdx
set num_monomers [lindex $argv 0]
set chainid [lindex $argv 1]

#set startidx [lindex $argv 1]
#set endidx [expr $startidx + $len]

mol delrep 0 0
#mol selection {index < 14}
#mol selection {index >= $startidx and index < $endidx}
mol selection {resid $chainid}
#mol representation VDW 0.8 26.0
mol representation Points
mol addrep 0

color Display Background white
mol modstyle 0 0 points


draw material Glossy

axes location off
display cuedensity 0.20000
display cuemode Exp2 
display shadows on
display ambientocclusion on

visualize 0 $num_monomers
