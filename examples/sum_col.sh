#!/bin/sh

file=$1
col=$2

awk -v col="$col" 'BEGIN{sum=0}{sum+=$col}END{printf("sum=%lf\n",sum);}' $file
