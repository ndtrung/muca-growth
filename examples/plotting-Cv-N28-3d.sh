gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,50"
set output "results-28.eps"
set encoding utf8

set key top left reverse Left samplen 2.0 font "Helvetica, 40"

set style line 1 lt 1 lc rgb "black"       pt 6 lw 4 ps 3
set style line 2 lt 1 lc rgb "red"        pt 5 lw 6 ps 2
set style line 3 lt 1 lc rgb "dark-green" pt 11 lw 6 ps 3
set style line 4 lt 1 lc rgb "black"       pt 9 lw 6 ps 3
set style line 5 lt 1 lc rgb "orange"     pt 13 lw 6 ps 3

W=3.0
H=1.6
set size W,H

set multiplot layout 2,1

#set xlabel "{/Helvetica-Italic E}"
#set ylabel "{/Helvetica-Italic ~g{.5-}}({/Helvetica-Italic E})" rotate by 0 font "Helvetica, 50" offset -1,0

set xlabel "E"
set ylabel "~g{.5-}(E)" rotate by 0 font "Helvetica, 50" offset -1,0

set mxtics 2
set mytics 2
set xtics scale 4 format "%g"

set bars 3
#set errorbars scale 2

set border lw 4
#set xrange [0:4]
#set yrange [:1]

set lmargin 10

set title "(a)"
set size W/2,H
set origin 0,0

N=28
exactZ=431645810810533376.0
sumgE=42.2704

set logscale y
set ytics scale 4 format "10^{%L}"

plot 'ave-gE-28-3d.txt' u 1:(\$4*exactZ) w lp ls 1 t "MUCA", '' u 1:(\$4*exactZ):(\$3*exactZ/sumgE) w errorbars ls 1 notitle, \
 'exact-N28-3d.dat' u 1:(\$2) w p ls 2 t "Exact"


set size W/4+0.2,H/4+0.2
set origin W/4-0.25,H/4

set xtics scale 4 format ""
unset xlabel
unset title
set yrange [1e-20:]
set ytics font "Helvetica, 40" (1e-20,1e-16,1e-12,1e-8,1e-4,1)
set ylabel "g(E)" rotate by 0 font "Helvetica, 40" offset 1,0
plot 'ave-gE-28-3d.txt' u 1:(\$2) w lp ls 1 ps 2 notitle
#, '' u 1:(\$2):(\$3) w errorbars ls 1 notitle


#set ylabel "{/Helvetica-Italic C}_v/{/Helvetica-Italic N}" rotate by 0 font "Helvetica, 50"
#set xlabel "{/Helvetica-Italic T}"

set ylabel "C_v/N" rotate by 0 font "Helvetica, 50"
set xlabel "T"

set key top right
set mxtics 2
set yrange [0:1.0]
set xrange [0:2.5]
set xtics scale 4 format "%0.1f"
set ytics 0.2 scale 4 format "%0.1f"
set size W/2,H
set origin W/2,0

unset logscale y

set title "(b)"
plot 'ave-Cv-28-3d.txt' u 1:(\$2/N) w p ls 1 t "MUCA", '' u 1:(\$2/N):(\$3/N) w errorbars ls 1 notitle,\
'Cv-exact-N28-3d.dat' u 1:(\$2/N) w l ls 2 t "Exact"

unset multiplot
EOF
